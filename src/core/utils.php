<?php

namespace Blacknet\Lib\Core;
use ParagonIE_Sodium_Core_Util;
use Blacknet\Lib\Exception\BlacknetException;
use SplFixedArray;

const hrp   = "blacknet";

abstract class Utils extends ParagonIE_Sodium_Core_Util
{

    protected static function toUint64Array($num)
    {
        return unpack("C*", pack("J", $num));
    }
    protected static function toUint32Array($num)
    {
        return unpack("C*", pack("N", $num));
    }
    protected static function toUint16Array($num)
    {
        return unpack("C*", pack("n", $num));
    }
    protected static function toUint8Array($num)
    {
        return array_slice(unpack("C*", pack("n", $num)), 1);
    }
    protected static function uint64ArrayToNumeric($arr)
    {
        return  (int) unpack("J", self::arrayToString($arr))[1];
    }
    protected static function uint32ArrayToNumeric($arr)
    {
        return  (int) unpack("N", self::arrayToString($arr))[1];
    }
    protected static function uint16ArrayToNumeric($arr)
    {
        return  (int) unpack("n", self::arrayToString($arr))[1];
    }
    protected static function uint8ArrayToNumeric($arr)
    {
        return  (int) unpack("C", self::arrayToString($arr))[1];
    }

    public static function SplFixedArrayToString(SplFixedArray $a)
    {
        $arr = $a->toArray();
        $c = $a->count();
        array_unshift($arr, str_repeat('C', $c));
        return (string) (call_user_func_array('pack', $arr));
    }
    public static function stringToSplFixedArray($str = '')
    {
        $values = unpack('C*', $str);
        return SplFixedArray::fromArray(array_values($values));
    }
    public static function arrayToSplFixedArray(array $arr)
    {
        return SplFixedArray::fromArray($arr);
    }
    public static function arrayToString(array $arr)
    {
        $c = count($arr);
        array_unshift($arr, str_repeat('C', $c));
        return (string) (call_user_func_array('pack', $arr));
    }
    public static function stringToArray($str = '')
    {
        $values = unpack('C*', $str);
        return array_values($values);
    }
    public static function numberOfLeadingZeros($i) {
        // HD, Figure 5-6
        if ($i == 0) {
            return 32;
        }
        $n = 1;
        if ($i>>16 == 0) {
            $n += 16;
            $i <<= 16;
        }
        if ($i>>24 == 0) {
            $n += 8;
            $i <<= 8;
        }
        if ($i>>28 == 0) {
            $n += 4;
            $i <<= 4;
        }
        if ($i>>30 == 0) {
            $n += 2;
            $i <<= 2;
        }
        $n -= $i >> 31;
        return $n;
    }
    public static function encodeVarInt($value) {
        $shift = 31 - self::numberOfLeadingZeros($value);
        $shift -= $shift % 7; // round down to nearest multiple of 7
        // SplFixedArray
        $buf = array();
        while(true) {
            if ($shift == 0) {
                break;
            }
            array_push($buf, $value>>$shift&0x7F);
            $shift -= 7;
        }
        array_push($buf, $value&0x7F|0x80);
        return $buf;
    }
    public static function decodeVarInt($arr) {
        $ret = 0;
        $offset = 0;
        while(true) {
            $v = $arr[$offset];
            $ret = $ret<<7 | ($v & 0x7F);
            if (($v&0x80) != 0) {
                break;
            }
            $offset++;
        }
        return $ret;
    }

    public static function publickey($account)
    {
        list ($gotHRP, $address) = \Bitwasp\Bech32\decode($account);
        if(strcmp($gotHRP, hrp) !== 0){
            throw new BlacknetException('Invalid hrp');
        }
        $decoded = \BitWasp\Bech32\convertBits($address, count($address), 5, 8, false);
        $program = '';
        foreach ($decoded as $char) {
            $program .= chr($char);
        }
        return $program;
    }

    public static function publickeyToHex($arr)
    {
        return strtoupper(bin2hex($arr));
    }

    public static function hexToPublickey($str)
    {
        return hex2bin(strtolower($str));
    }

    public static function toHex($arr)
    {
        return strtoupper(bin2hex($arr));
    }

    public static function toByte($str)
    {
        return hex2bin(strtolower($str));
    }
}

/*
public static function encodeVarInt($value) {
    $shift = 31 - self::numberOfLeadingZeros($value);
    $shift -= $shift % 7; // round down to nearest multiple of 7
    // SplFixedArray
    $buf = array();
    while(true) {
        if ($shift == 0) {
            break;
        }
        array_push($buf, $value>>$shift&0x7F);
        $shift -= 7;
    }
    array_push($buf, $value&0x7F|0x80);
    return join(array_map("chr", $buf));
}
public static function decodeVarInt($arr) {
    $ret = 0;
    $offset = 0;
    while(true) {
        $v = self::chrToInt($arr[$offset]);
        $ret = $ret<<7 | ($v & 0x7F);
        if (($v&0x80) != 0) {
            break;
        }
        $offset++;
    }
    return $ret;
}
*/