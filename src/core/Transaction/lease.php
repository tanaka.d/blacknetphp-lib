<?php

namespace Blacknet\Lib\Core\Transaction;
use Blacknet\Lib\Core\Utils;

class Lease extends Utils{
    var $amount; //uint64
    var $to; //string
    function __construct($amount, $to) {
        $this->amount  = intval($amount);
        $this->to      = self::publickeyToHex(self::publickey($to));
    }
    public function serialize(){
        $amount = self::toUint64Array($this->amount);
        $to = self::stringToArray(self::hexToPublickey($this->to));
        return array_merge(
            $amount,
            $to
        );
    }
    public static function derialize(array $arr){
        $amount = self::uint64ArrayToNumeric(array_slice($arr, 0, 8));
        $to = self::publickeyToHex(self::arrayToString(array_slice($arr, 8, 32)));
        return new Lease($amount, $to);
    }
}
