<?php

namespace Blacknet\Lib\Core\Transaction;
use Blacknet\Lib\Core\Utils;

class CancelLease extends Utils{
    var $amount; //uint64
    var $to; //string
    var $height; //uint32
    function __construct($amount, $to, $height) {
        $this->amount  = intval($amount);
        $this->to      = self::publickeyToHex(self::publickey($to));
        $this->height  = intval($height);
    }
    public function serialize(){
        $amount = self::toUint64Array($this->amount);
        $to     = self::stringToArray(self::hexToPublickey($this->to));
        $height = self::toUint32Array($this->height);
        return array_merge(
            $amount,
            $to,
            $height
        );
    }
    public static function derialize(array $arr){
        $amount = self::uint64ArrayToNumeric(array_slice($arr, 0, 8));
        $to = self::publickeyToHex(self::arrayToString(array_slice($arr, 8, 32)));
        $height = self::uint32ArrayToNumeric(array_slice($arr, 40));
        return new CancelLease($amount, $to, $height);
    }
}
