<?php

namespace Blacknet\Lib\Core;
// use SplFixedArray;

class Hash extends Utils{
    var $hash; //SplFixedArray
    function __construct( array $hash ) {
        if (!empty($hash)) {
            $this->hash = $hash;
        }else{
            $this->hash = array_fill(0,32,0);
        }
    }
    public function bytes(){
        return $this->hash;
    }
    public function string(){
        return self::arrayToString($this->hash);
    }
    public function length(){
        return count($this->hash);
    }
    public static function empty(){
        return new Hash(array_fill(0,32,0));
    }
}