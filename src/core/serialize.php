<?php

namespace Blacknet\Lib\Core;
use Blacknet\Lib\Core\Utils;
use Blacknet\Lib\Core\Transaction\Transfer;
use Blacknet\Lib\Core\Transaction\Lease;
use Blacknet\Lib\Core\Transaction\Transaction;
use Blacknet\Lib\Core\Transaction\CancelLease;
use Blacknet\Lib\Core\Transaction\WithdrawFromLease;

class Serialize extends Utils
{
    var $jsonrpc; // JSONRPC
    function __construct(JSONRPC $jsonrpc = null) {
        if(empty($jsonrpc)){
            $this->jsonrpc = new JSONRPC();
        }else{
            $this->jsonrpc = $jsonrpc;
        }
    }
    /**
     * @param array $data map[fee=>int, amount=>int, message=>string, to=>string, from=>string, encrypted=>int]
     * @return string string
     */
    public function transfer(array $data){
        $seq = $this->jsonrpc->getSeq($data["from"]);
        $m = new Message(Message::$PLAIN, $data["message"]);
        $d = new Transfer($data["amount"], $data["to"], $m);
        $ts = new Transaction($data["from"], $seq, Hash::empty(), $data["fee"], 0, $d->serialize());
        return new Body(200, self::toHex(self::arrayToString($ts->serialize())));
    }

    /**
     * @param array $data map[fee=>int, amount=>int, to=>string, from=>string]
     * @return string string
     */
    public function lease(array $data){
        $seq = $this->jsonrpc->getSeq($data["from"]);
        $d = new Lease($data["amount"], $data["to"]);
        $ts = new Transaction($data["from"], $seq, Hash::empty(), $data["fee"], 2, $d->serialize());
        return new Body(200, self::toHex(self::arrayToString($ts->serialize())));
    }
    /**
     * @param array $data map[fee=>int, amount=>int, to=>string, from=>string, height=>int]
     * @return string string
     */
    public function cancelLease(array $data){
        $seq = $this->jsonrpc->getSeq($data["from"]);
        $d = new CancelLease($data["amount"], $data["to"], $data["height"]);
        $ts = new Transaction($data["from"], $seq, Hash::empty(), $data["fee"], 3, $d->serialize());
        return new Body(200, self::toHex(self::arrayToString($ts->serialize())));
    }
    /**
     * @param array $data map[fee=>int, amount=>int, to=>string, from=>string, height=>int, withdraw=>int]
     * @return string string
     */
    public function withdrawFromLease(array $data){
        $seq = $this->jsonrpc->getSeq($data["from"]);
        $d = new WithdrawFromLease($data["withdraw"], $data["amount"], $data["to"], $data["height"]);
        $ts = new Transaction($data["from"], $seq, Hash::empty(), $data["fee"], 11, $d->serialize());
        return new Body(200, self::toHex(self::arrayToString($ts->serialize())));
    }
}